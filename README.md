# Burg Food

---
## Overview

Burg Food is an app that allows Burgmann College residents to make lunch and dinner orders from the convenience of their phones!


## Download
---

This app can be found live on the Play Store. 

[https://play.google.com/store/apps/details?id=christopher.seidl.burgfood](https://play.google.com/store/apps/details?id=christopher.seidl.burgfood)

## Screenshots
---
![Screen Grab](BurgFoodScreenGrab.gif)
