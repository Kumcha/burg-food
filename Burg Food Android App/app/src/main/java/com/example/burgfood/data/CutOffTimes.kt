package com.example.burgfood.data

/**
 *
 * CutOffTimes - Place to define cut off and collection times.
 *
 */


enum class CutOffTimes(val hours: Int, val minutes: Int) {

    packedLunchOrderBy(6,0),
    packedLunchCollectionTime(7,0),
    hotLunchOrderBy(11,0),
    hotLunchCollectionTime(12,0),
    dinnerOrderBy(17,0),
    dinnerCollectionTime(18,0),


}