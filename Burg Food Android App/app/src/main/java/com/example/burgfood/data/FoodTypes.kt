package com.example.burgfood.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *
 * FoodTypes - Defines all the possible items that can be ordered. Defines the relevant image, order
 *             string and the type. The typ refers to if there can be multiple of the item or only
 *             one.
 *
 */

@Parcelize
enum class FoodTypes(var resource : FoodImage, var string : String, var type : Int) : Parcelable {
    // Type 1 - Unique
    // Packed
    ChickenMayoSandwich(FoodImage.Chicken, "Chicken & \nMayo Sandwich", 1),
    VegemiteCheeseSandwich(FoodImage.Sandwich, "Vegemite & \nCheese Sandwich", 1),
    TunaSandwich(FoodImage.Fish, "Tuna Sandwich", 1),
    BeefMustardLettuceSandwich(FoodImage.Beef, "Beef,\nMustard & \nLettuce Sandwich", 1),
    CheeseTomatoSandwich(FoodImage.Sandwich, "Cheese & \nTomato Sandwich", 1),
    SaladMargarineSandwich(FoodImage.Salad, "Salad & \nMargarine Sandwich", 1),

    // Hot & Dinner Mains
    BeefPreference(FoodImage.Beef, "Beef \nPreference", 1),
    ChickenPreference(FoodImage.Chicken, "Chicken \nPreference", 1),
    PorkPreference(FoodImage.Pork, "Pork \nPreference", 1),
    VegetarianPreference(FoodImage.Salad, "Vegetarian \nPreference", 1),
    FishPreference(FoodImage.Fish, "Fish \nPreference", 1),

    //Type 0 -- Not unique
    Pasta(FoodImage.Pasta, "Pasta", 0),
    Salad(FoodImage.Salad, "Salad", 0),
    Dessert(FoodImage.Desert, "Dessert", 0);

    override fun toString(): String {
        return super.toString()
    }
}