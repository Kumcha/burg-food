package com.example.burgfood.orderDisplays

import android.content.Intent
import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.burgfood.R
import com.example.burgfood.data.CutOffTimes
import com.example.burgfood.data.FoodTypes
import com.example.burgfood.data.Order
import kotlinx.android.synthetic.main.menu_fragment.*
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * MenuFragment - Handles each tab.
 *
 */

class MenuFragment(private var data: Order, private val cutoffTime: Date, private val orderType: String) : Fragment() {
    private var currentTime: Date = Calendar.getInstance().time
    private var today = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        this.retainInstance = true

        // Start the activity
        return inflater.inflate(R.layout.menu_fragment, container, false)

        // Toast.makeText(activity, "Its toast!", Toast.LENGTH_SHORT).show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Determine size of 1dp
        val scale = (resources.displayMetrics.density.toInt())


        // Set onclick listener
        confirmButton.setOnClickListener{confirmOrder(data)}

        // Check if there are any selected items if so bring up the confirm button.
        if (data.selectedType1 != null || data.selectedType0.isNotEmpty()) {
            confirmButton.layoutParams.height = 75 * scale
        }

        val format = SimpleDateFormat("h a")

        // Determine cutoff time & set appropriate text based on completion time.
        if (currentTime.after(cutoffTime)) {
            when (data.orderType) {
                0 -> {

                    val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.packedLunchCollectionTime.hours, CutOffTimes.packedLunchCollectionTime.minutes)
                    confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Tomorrow"
                }
                1 -> {

                    val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.hotLunchCollectionTime.hours, CutOffTimes.hotLunchCollectionTime.minutes)
                    confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Tomorrow"
                }
                else -> {

                    val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.dinnerCollectionTime.hours, CutOffTimes.dinnerCollectionTime.minutes)
                    confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Tomorrow"
                }
            }

            today = false

        } else {

            when (data.orderType) {
                0 -> {

                    val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.packedLunchCollectionTime.hours, CutOffTimes.packedLunchCollectionTime.minutes)
                    confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Today"
                }
                1 -> {

                    val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.hotLunchCollectionTime.hours, CutOffTimes.hotLunchCollectionTime.minutes)
                    confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Today"
                }
                else -> {

                    val completionDate = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.dinnerCollectionTime.hours, CutOffTimes.dinnerCollectionTime.minutes)
                    confirmButton.text = "Confirm Order for " + format.format(completionDate) +  " Today"
                }
            }

            today = true
        }

        // Set out recycler view
        packedLunchRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ItemAdapter(data, context, confirmButton, scale, cutoffTime)
        }
    }

    // Onclick for the order button - order is what has been ordered
    private fun confirmOrder(order: Order) {
        // Log.d("myTag", "click confirm order")

        // a list of all the order items
        val orderItems = arrayListOf<FoodTypes>()

        // Add the type 1 item if there is one
        if (order.selectedType1 != null) {
            orderItems.add(order.selectedType1!!.type)
        }

        // add all type 0 items
        order.selectedType0.forEach {
            orderItems.add(it.value.type)
        }

        // put the order time for order and the type of order into the intent and move to the next screen
        val intent = Intent(context, com.example.burgfood.ConfirmOrderActivity::class.java).apply {
            putExtra("order", orderItems)
            putExtra("today", today)
            putExtra("caller", orderType)
        }
        startActivity(intent)

        // Log.d("myTag", order.selectedType1?.type?.string)
        // Log.d("myTag", order.selectedType0.count().toString())
    }
}