package com.example.burgfood.orderHistory

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.burgfood.R
import com.example.burgfood.data.OrderHistoryItem
import com.google.android.material.snackbar.Snackbar
import com.parse.ParseObject
import com.parse.ParseQuery
import java.text.SimpleDateFormat

/**
 *
 * OrderItemAdapter - Adapter for the order history recycler view. Manages the display of data and
 *                    some aspects of data persistence / communication with the DB
 *
 */

class OrderItemAdapter(
    var data: ArrayList<OrderHistoryItem>,
    private val context: Context,
    private val sharedpref: SharedPreferences
): RecyclerView.Adapter<OrderItemAdapter.ItemViewHolder>() {

    // Keep track of the last deleted item so we can undo it.
    private var removedPosition: Int = 0
    private var removedItem: OrderHistoryItem? = null

    // An inner class to manage each row
    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val backgroundContainer : ImageView = view.findViewById(R.id.backgroundOrderHistory)
        val orderType : TextView = view.findViewById(R.id.orderTypeTextView)
        val orderItems : TextView = view.findViewById(R.id.orderContentTextView)
        val orderDate : TextView = view.findViewById(R.id.orderDateTextView)
    }


    // Removing an item - Sort of an onclick for the delete swipe
    fun removeItem(position: Int, viewHolder: RecyclerView.ViewHolder) {
        // store it as the last deleted item
        removedItem = data[position]
        removedPosition = position

        // remove the item
        data.removeAt(position)
        notifyItemRemoved(position)

        val query = ParseQuery.getQuery<ParseObject>(removedItem!!.getOrderClassName())
        query.getInBackground(removedItem!!.orderInstanceId
        ) { entity, e ->
            if (e == null) {
                if (entity != null) {

                    // ** Save the data in case the user leaves the app without hitting back **
                    // orderString to be stored
                    var orderString = ""
                    // create the string in format ",ID-Ordertype,ID-Ordertype,ID-Ordertype"
                    data.forEach {
                        orderString = orderString + "," + it.orderInstanceId + "-" + it.orderType
                    }
                    // remove the first ","
                    orderString = orderString.drop(0)
                    // put in shared prefs.
                    val sharedPref = sharedpref
                    with(sharedPref.edit()) {
                        putString("orderIDHistory", orderString)
                        commit()
                    }

                    // Send to backend eventually.
                    entity.deleteEventually()
                } else {
                    Log.d("Order History Deletion" , "Entity not found")
                }
            } else {
                Log.d("Order History Deletion" , e.message)
            }
        }

        // make a snackbar
        val snackbar  = Snackbar.make(viewHolder.itemView, removedItem!!.orderType + " Removed", Snackbar.LENGTH_LONG)

        snackbar.show()
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = data[position]
        val format = SimpleDateFormat("dd MMM yyy")

        // Set correct image
        holder.backgroundContainer.setImageResource(item.foodItems[0].resource.resource)
        holder.backgroundContainer.setColorFilter(item.foodItems[0].resource.overlay, android.graphics.PorterDuff.Mode.MULTIPLY)
        holder.orderType.text = item.orderType


        holder.orderItems.text = item.getOrderString()
        holder.orderDate.text =  format.format(item.orderTime)
    }

    // used to change the data
    fun notifyData(myList: ArrayList<OrderHistoryItem>) {

        // new data
        data = myList
        notifyDataSetChanged()
        // Log.d("notifyData ", myList.size.toString() + "")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder { return ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.order_history_item_layout, parent, false)) }
    override fun getItemCount(): Int { return data.size }
}