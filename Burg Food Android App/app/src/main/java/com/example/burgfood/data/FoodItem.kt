package com.example.burgfood.data

/**
 *
 * FoodItem - An object that can encapsulate the enum and store if it has been selected currently
 *
 */

data class FoodItem(var index : Int, val type : FoodTypes) {
    var selected = false
}