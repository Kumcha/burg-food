package com.example.burgfood.orderDisplays

import android.icu.util.Calendar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.burgfood.data.CutOffTimes
import com.example.burgfood.data.FoodItem
import com.example.burgfood.data.FoodTypes
import com.example.burgfood.data.Order
import java.util.*

/**
 *
 * SectionsPagerAdapter - Creates and manages the fragments (the tabs). Here the menu for each order
 *                        type is defined along with the cut off times. Cut off times are also
 *                        defined in OrderHistoryActivity. The collection times are defined in
 *                        MenuFragment
 *
 */

class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private var currentTime: Date = Calendar.getInstance().time

    private val packedLunchData = arrayOf(
        FoodItem( 0 , FoodTypes.ChickenMayoSandwich),
        FoodItem( 1, FoodTypes.VegemiteCheeseSandwich),
        FoodItem( 2, FoodTypes.TunaSandwich),
        FoodItem( 3, FoodTypes.BeefMustardLettuceSandwich),
        FoodItem(4, FoodTypes.CheeseTomatoSandwich),
        FoodItem(5, FoodTypes.SaladMargarineSandwich)
    )
    private val packedLunchCutOffTime = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.packedLunchOrderBy.hours, CutOffTimes.packedLunchOrderBy.minutes)
    private val packedLunch = Order(packedLunchData, 0)

    private val hotLunchData = arrayOf(
        FoodItem(0, FoodTypes.BeefPreference),
        FoodItem(1, FoodTypes.ChickenPreference),
        FoodItem( 2, FoodTypes.PorkPreference),
        FoodItem( 3 , FoodTypes.VegetarianPreference),
        FoodItem( 4, FoodTypes.Pasta),
        FoodItem(5, FoodTypes.Salad)
    )
    private val hotLunch = Order(hotLunchData, 1)
    private val hotLunchCutOffTime = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.hotLunchOrderBy.hours, CutOffTimes.hotLunchOrderBy.minutes)

    private val dinnerData = arrayOf(
        FoodItem(0 ,FoodTypes.BeefPreference),
        FoodItem( 1, FoodTypes.ChickenPreference),
        FoodItem( 2, FoodTypes.FishPreference),
        FoodItem( 3, FoodTypes.PorkPreference),
        FoodItem( 4, FoodTypes.VegetarianPreference),
        FoodItem( 5, FoodTypes.Pasta),
        FoodItem( 6, FoodTypes.Salad),
        FoodItem( 7, FoodTypes.Dessert)
    )
    private val dinner = Order(dinnerData, 2)
    private val dinnerCutOffTime = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.dinnerOrderBy.hours, CutOffTimes.dinnerOrderBy.minutes)

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> MenuFragment(packedLunch, packedLunchCutOffTime, "Packed Lunch")
            1 -> MenuFragment(hotLunch, hotLunchCutOffTime, "Hot Lunch")
            else -> {
                MenuFragment(dinner, dinnerCutOffTime, "Dinner")
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }


    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Packed Lunch"
            1 -> "Hot Lunch"
            else -> {
                return "Dinner"
            }
        }
    }


}