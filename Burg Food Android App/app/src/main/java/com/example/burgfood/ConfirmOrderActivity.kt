package com.example.burgfood

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.burgfood.data.FoodTypes
import com.parse.ParseObject
import kotlinx.android.synthetic.main.activity_confirm_order.*
import org.json.JSONArray
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 *
 * ConfirmOrderActivity - Where the user is taken to allow them to confirm their order and attempt
 *                        to send the order to the DB.
 *
 */

class ConfirmOrderActivity : AppCompatActivity() {

    // Information about the order
    private var orderType = ""
    var name: String? = ""
    private var mealCode: String? = ""
    var order = arrayListOf<FoodTypes>()
    private var orderArrForDB = arrayListOf<String>()
    private var orderToBeCompleatedToday = true
    private var dateForCollection = ""
    private var dietaryReq = true
    private var orderId = ""

    // Used to disable the back button while attempting to send to the DB
    private var currentlySending = false

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_order)

        // Ensure that the sending animation is not visible
        progressBar.visibility = View.GONE

        // Enable animated layout changes
        findViewById<ConstraintLayout>(R.id.confirmOrderConstrainLayout).layoutTransition.enableTransitionType(
            LayoutTransition.CHANGING)

        // Get data from the intent
        this.order = intent.getParcelableArrayListExtra<FoodTypes>("order")
        this.orderToBeCompleatedToday = intent.getBooleanExtra("today", true)
        this.orderType = intent.getStringExtra("caller")


        // Retrieve username, meal code & diet options
        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        val defaultValue = "Error"
        name = sharedPref.getString("Username", defaultValue)
        mealCode = sharedPref.getString("MealCode", defaultValue)
        dietaryReq = sharedPref.getBoolean("DietaryReq", true)

        // Set calender time and instance for today and tomorrow
        val calendar = Calendar.getInstance()
        val todayCal = calendar.time
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrowCal = calendar.time

        // Format the date appropriately
        val dateFormat: DateFormat = SimpleDateFormat("dd-MM-yy")
        val todayAsString = dateFormat.format(todayCal).toString().replace("-", ".")
        val tomorrowAsString = dateFormat.format(tomorrowCal).replace("-", ".")

        // Create the dateForCollection, in the correct format to send.
        dateForCollection = if (orderToBeCompleatedToday) {
            todayAsString
            // Log.d("MyTag", todayAsString)
        } else {
            tomorrowAsString
            // Log.d("MyTag", tomorrowAsString)
        }

        // Create the order string to display by adding all the order items together.
        var orderString = ""
        order.forEach {
            var stringToAdd = it.string

            // Remove any new lines
            stringToAdd = stringToAdd.replace("\n", " ")

            orderArrForDB.add(stringToAdd)
            orderString += "$stringToAdd, "
        }
        orderString = orderString.dropLast(2)


        // Chooses what to display Today or Tomorrow based on the order they made and the type of order.
        if (orderToBeCompleatedToday) {
            orderTextView.text = "Confirm Your $orderType Order: $orderString for Today"
        } else {
            orderTextView.text =
                "Confirm Your $orderType Order: $orderString for Tomorrow"
        }

        // Get the relevant image for the first item in the order
        orderImageView.setImageResource(order[0].resource.resource)
        orderImageView.setColorFilter(
            order[0].resource.overlay,
            android.graphics.PorterDuff.Mode.MULTIPLY
        )

        // Cancel button does the same thing as back button
        cancelButton.setOnClickListener { onBackPressed() }
    }

    // Back Button
    override fun onBackPressed() {
        // Disable if something is being sent to the DB
        if (!currentlySending) {
            super.onBackPressed()
        }
    }

    // Confirm Button - Updated the view and tries to send the order
    fun confirmOrder(view: View){

        // Ensure that the back button cannot be pressed
        currentlySending = true

        // Show the loading bar and hide all the buttons and change the text
        progressBar.visibility = View.VISIBLE
        cancelButton.visibility = View.INVISIBLE
        confirmButton.visibility = View.INVISIBLE
        orderTextView.text = "Sending Order"

        // Create the Name of the DB and get the correct class form the DB
        val objectName = getClassNameFromOrderTypeString(orderType)
        val entity = ParseObject(objectName)

        // Make all the JSON Arrays and put the values in them
        val nameArr = JSONArray()
        nameArr.put(name)
        val roomArr = JSONArray()
        roomArr.put(mealCode)
        val hasDietaryArr = JSONArray()
        hasDietaryArr.put(dietaryReq)
        val dateForCollectionArr = JSONArray()
        dateForCollectionArr.put(dateForCollection)
        val orderArr = JSONArray()
        // Loop through and put all the orders in
        orderArrForDB.forEach { orderArr.put(it)}

        // Put the JSON Arrays in the PARSE object
        entity.put("order", JSONArray().put(orderArr))
        entity.put("dateForCollection", dateForCollectionArr)
        entity.put("name", nameArr)
        entity.put("room", roomArr)
        entity.put("hasDietary", hasDietaryArr)

        // Saves the new object.
        entity.saveInBackground { e ->
            // When the Order has been sent or times out
            if (e == null) {
                // For concurrency
                val handler = Handler()

                orderId = entity.objectId

                // Done concurrently so the delays can be used to allow sufficient time for the user to take in the loading times
                // Show the success
                handler.postDelayed({ orderSuccessful() }, 2000)
                // Go back to the log in page
                handler.postDelayed({ backToLogIn() }, 3000)

            } else { // Error
                currentlySending = false
                errorDialogue()
            }
        }

    }

    // Take the user back to the log in screen
    private fun backToLogIn() {
        // Back to Log In View
        val intent = Intent(this, LogInActivity::class.java).apply {}
        startActivity(intent)
    }

    // What to do when it sends successfully - Change text, Remove loading, set the green tick
    private fun orderSuccessful() {
        orderTextView.text = "Order Sent"
        progressBar.visibility = View.GONE
        orderImageView.setImageResource(R.drawable.tickgreen)

        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        val prevOrderID = sharedPref.getString("orderIDHistory", "")

        with(sharedPref.edit()) {
            putString("orderIDHistory", "$prevOrderID,$orderId-$orderType")
            commit()
        }

    }

    // What to do when sending to the DB results in an Error - Hide loading bar, display an alert, then reset the activity
    private fun errorDialogue() {

        progressBar.visibility = View.INVISIBLE

        // The alert code
        val dialogBuilder = AlertDialog.Builder(this).setMessage("Check you are connected to the internet and try again.")
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action - resets the current activity
            .setPositiveButton("Ok") { dialog, id -> resetPage() }

        val alert = dialogBuilder.create()
        alert.setTitle("Order could not be sent to the kitchen")
        alert.show()
    }

    // Resets the confirm order page so the user can try again
    private fun resetPage() {

        progressBar.visibility = View.GONE
        cancelButton.visibility = View.VISIBLE
        confirmButton.visibility = View.VISIBLE

        // Remake orderString
        var orderString = ""
        order.forEach {
            var stringToAdd = it.string
            stringToAdd = stringToAdd.replace("\n", " ")
            orderArrForDB.add(stringToAdd)
            orderString += "$stringToAdd, "
        }
        orderString = orderString.dropLast(2)


        // Chooses what to display Today or Tomorrow based on the order they made and the type of order.
        if (orderToBeCompleatedToday) {
            orderTextView.text = "Confirm Your $orderType Order: $orderString for Today"
        } else {
            orderTextView.text =
                "Confirm Your $orderType Order: $orderString for Tomorrow"
        }

        // Reset image
        orderImageView.setImageResource(order[0].resource.resource)
        orderImageView.setColorFilter(order[0].resource.overlay, android.graphics.PorterDuff.Mode.MULTIPLY)
    }


    private fun getClassNameFromOrderTypeString(orderType: String) : String {
        return when (orderType) {
            "Packed Lunch" -> "PackedLunch"
            "Hot Lunch" -> "HotLunch"
            else -> "Dinner"
        }
    }
}
