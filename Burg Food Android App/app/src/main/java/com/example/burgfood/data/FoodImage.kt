package com.example.burgfood.data

import android.graphics.Color
import com.example.burgfood.R

/**
 *
 * FoodImage - Defines the overlay/filters for each image resource
 *
 */

enum class FoodImage(var resource : Int, var overlay : Int) {
    Beef(R.drawable.beef, Color.rgb(219, 169, 154)),
    Chicken(R.drawable.chicken, Color.rgb(255, 209, 196)),
    Desert(R.drawable.desert, Color.rgb(230, 188, 204)),
    Fish(R.drawable.fish, Color.rgb(182, 204, 182)),
    Pasta(R.drawable.pasta, Color.rgb(255, 197, 153)),
    Pork(R.drawable.pork, Color.rgb(250, 195, 215)),
    Salad(R.drawable.salad, Color.rgb(183, 203, 232)),
    Sandwich(R.drawable.sandwich, Color.rgb(250, 183, 167));
}