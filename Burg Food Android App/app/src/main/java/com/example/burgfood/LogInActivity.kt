package com.example.burgfood

import android.animation.LayoutTransition
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.burgfood.orderDisplays.SelectMealViewActivity
import com.example.burgfood.orderHistory.OrderHistoryActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_log_in.*

/**
 *
 * LogInActivity - Where the user enter their name, meal code and their dietary requirements. Also
 *                 where the user can navigate to their order history.
 *
 */

class LogInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        // Enable animations for layout changes
        findViewById<ConstraintLayout>(R.id.mainConstraintLayout).layoutTransition.enableTransitionType(LayoutTransition.CHANGING)

        // Retrieve the username, meal code & diet options that has previously been entered
        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        val defaultValue = ""
        val username = sharedPref.getString("Username", defaultValue)
        val mealCode = sharedPref.getString("MealCode", defaultValue)
        val dietaryReq = sharedPref.getBoolean("DietaryReq", true)

        // Set the retrieved values to the screen
        findViewById<TextInputEditText>(R.id.nameTextFieldInner).setText(username)
        findViewById<TextInputEditText>(R.id.mealCodeTextFieldInner).setText(mealCode)
        findViewById<Switch>(R.id.dietSwitch).isChecked = dietaryReq
        dietHandler(findViewById<ConstraintLayout>(R.id.mainConstraintLayout))

    }
    
    // Handles the click of the 'Continue' Button
    fun logIn(view: View) {
        val nameTextField = findViewById<TextInputLayout>(R.id.nameTextField)
        val mealCodeTextField = findViewById<TextInputLayout>(R.id.mealCodeTextField)

        // Get the users inputs
        val username = findViewById<TextInputEditText>(R.id.nameTextFieldInner).text.toString()
        val mealCode = findViewById<TextInputEditText>(R.id.mealCodeTextFieldInner).text.toString()

        // Validate username and meal code
        when {
            // Validation
            username == "" -> { nameTextField.error = "Please Enter A Name" }
            username.split(" ").count() < 2 -> { nameTextField.error = "Please Enter Your Full Name" }
            mealCode == "" -> {
                // Reset the error notification for the username and show an error for the meal code
                nameTextField.error = null
                mealCodeTextField.error = "Please Enter A Meal Code"
            }

            else -> { // If validated

                // Reset the error notifications
                nameTextField.error = null
                mealCodeTextField.error = null

                // Put Username in shared preference to save it.
                val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
                with(sharedPref.edit()) {
                    putString("Username", username)
                    putString("MealCode", mealCode)
                    putBoolean("DietaryReq", dietSwitch.isChecked)
                    commit()
                }

                // Next
                val intent = Intent(this, SelectMealViewActivity::class.java).apply {}
                startActivity(intent)
            }
        }


    }

    // Handles the clicking of the dietary preferences switch
    fun dietHandler(view : View) {
        if (dietSwitch.isChecked) {
            dietText.text = "I have dietary requirements"
        } else {
            dietText.text = "I have no dietary requirements"
        }
    }

    // Handles click of the 'knife and fork icon' which leads to order history
    fun orderHistoryOnclick(view: View) {
        val intent = Intent(this, OrderHistoryActivity::class.java).apply {}
        startActivity(intent)
    }

    // Disable back button
    override fun onBackPressed() {}

}
