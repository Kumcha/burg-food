package com.example.burgfood.orderHistory

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.icu.util.Calendar
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.burgfood.R
import com.example.burgfood.data.CutOffTimes
import com.example.burgfood.data.FoodTypes
import com.example.burgfood.data.OrderHistoryItem
import com.parse.ParseObject
import com.parse.ParseQuery
import kotlinx.android.synthetic.main.activity_order_history.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 *
 * OrderHistoryActivity - Displays the past orders that are still pending that were made from this
 *                        device. Allows the user to delete those orders.
 *
 */

class OrderHistoryActivity : AppCompatActivity() {

    // order history data in object form
    var orderHistory: ArrayList<OrderHistoryItem> = ArrayList()

    // Store reference to the itemAdapter so it can be accessed later in the program.
    var itemAdapter: OrderItemAdapter? = null

    // Color and icon for row deletion
    private lateinit var colorDrawableBackground: ColorDrawable
    private lateinit var deleteIcon: Drawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_history)

        // get the list of order ids as a string in format "ID-OrderType,ID-OrderType,ID-OrderType"
        val sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE) ?: return
        val prevOrderID = sharedPref.getString("orderIDHistory", "")

        // split it to ["ID-OrderType","ID-OrderType"]
        val arrayOfOrderAndType = prevOrderID!!.split(",").drop(1)
        Log.d("MyTag", "Order list is: $arrayOfOrderAndType")

        // Keep track of how many query we need to make
        val querysMade = arrayOfOrderAndType.size
        var querysReceived = 0

        // Get order data for each item - This is done by querying the DB using the orderID that was stored in the shared preferences
        arrayOfOrderAndType.forEach{
            // Split to split into ["ID",OrderType]
            val entry = it.split("-")

            // Set up the query, get the name of the class/db by removing the space in the centre of the OrderType.
            val query = ParseQuery.getQuery<ParseObject>(entry[1].replace(" ", ""))
            query.getInBackground(entry[0]) { result, e ->
                if (e == null) { // when we get a successful return

                    // increment counter to keep track of how many querys we received
                    querysReceived += 1

                    // get an array of all the items that are part of this order
                    val orderList = result.getJSONArray("order")?.getJSONArray(0)
                    val orderListArrayList:ArrayList<FoodTypes>  = ArrayList()

                    // Loop through the items in the order and get the relevant food object/enum
                    if (orderList != null) {
                        for (i in 0 until orderList.length()) {

                            // Format the order string in a way that we can use it to search fot he foodType enum.
                            var orderString = orderList.getString(i).replace("&", "")
                            orderString = orderString.replace(" ", "")
                            orderString = orderString.replace(",", "")

                            orderListArrayList.add(FoodTypes.valueOf(orderString))

                            Log.d("MyTag", "Order is: $orderString")
                        }
                    } else {
                        Log.d("MyTag", "Query Error : " + result.objectId)
                    }

                    // Get the date
                    val dateString = result.getJSONArray("dateForCollection")?.getString(0)

                    // Format the date into a date object
                    val date = SimpleDateFormat("dd.MM.yy").parse(dateString)

                    // Check if its already past the cut off time
                    // Time instances for today and tomorrow
                    val currentTime = Calendar.getInstance().time
                    val calendar = java.util.Calendar.getInstance()
                    val todayCal = calendar.time
                    calendar.add(java.util.Calendar.DAY_OF_YEAR, 1)
                    val tomorrowCal = calendar.time

                    // From the cut off time Decide what date to use as the cut off
                    val packedLunchCutOffTime = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.packedLunchOrderBy.hours, CutOffTimes.packedLunchOrderBy.minutes)
                    if (currentTime.after(packedLunchCutOffTime)) {
                        packedLunchCutOffTime.date = tomorrowCal.date
                    }

                    val hotLunchCutOffTime = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.hotLunchOrderBy.hours, CutOffTimes.hotLunchOrderBy.minutes)
                    if (currentTime.after(hotLunchCutOffTime)) {
                        hotLunchCutOffTime.date = tomorrowCal.date
                    }

                    val dinnerCutOffTime = Date(currentTime.year, currentTime.month, currentTime.date, CutOffTimes.dinnerOrderBy.hours, CutOffTimes.dinnerOrderBy.minutes)
                    if (currentTime.after(dinnerCutOffTime)) {
                        dinnerCutOffTime.date = tomorrowCal.date
                    }

                    val appropriateCutOffTime: Date

                    when (classNameSwitcher(result.className)) {
                        "Packed Lunch" -> {
                            appropriateCutOffTime = packedLunchCutOffTime
                            date.hours = packedLunchCutOffTime.hours
                            date.minutes = packedLunchCutOffTime.minutes
                        }
                        "Hot Lunch" -> {
                            appropriateCutOffTime = hotLunchCutOffTime
                            date.hours = hotLunchCutOffTime.hours
                            date.minutes = hotLunchCutOffTime.minutes
                        }

                        else -> { // 'Dinner'
                            appropriateCutOffTime = dinnerCutOffTime
                            date.hours = dinnerCutOffTime.hours
                            date.minutes = dinnerCutOffTime.minutes
                        }
                    }

                    if (!date.before(appropriateCutOffTime) ) {
                        // Create the new Order object and add it to our data
                        orderHistory.add(
                            OrderHistoryItem(
                                classNameSwitcher(result.className),
                                result.objectId,
                                orderListArrayList,
                                date
                            )
                        )
                    }

                    // Check if all the querys have been received
                    if (querysReceived >= querysMade) {
                        displayOrders()
                    }

                    // Log.d("MyTag", "Order is: " + result.getJSONArray("order").toString())
                } else {

                    // something went wrong
                    Log.d("MyTag", "Query Error : " + e.message)

                    querysReceived += 1
                    if (querysReceived >= querysMade) {
                        displayOrders()
                    }
                }
            }
        }

        // If there are no orders
        if (arrayOfOrderAndType.isEmpty()) {
            emptyMsgTextView.text = "You have no pending orders."
            orderHistoryProgressBar.visibility = View.INVISIBLE

        }

        // **** Setting up the row deletion **** - From Online tutorial https://www.youtube.com/watch?v=eEonjkmox-0

        // Set up the swipe to delete resources
        colorDrawableBackground = ColorDrawable(Color.parseColor("#ff0000"))
        deleteIcon = ContextCompat.getDrawable(this, R.drawable.binwhite)!!


        // onItemTouch
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, viewHolder2: RecyclerView.ViewHolder): Boolean { return false }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDirection: Int) {

                // Notify the item adapter of the input
                itemAdapter?.removeItem(viewHolder.adapterPosition, viewHolder)

                // Check if there are any orders left
                if  (orderHistory.isEmpty()) {
                    emptyMsgTextView.text = "You have no pending orders."
                }
            }

            // Stuff from the tutorial to animate the screen
            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val itemView = viewHolder.itemView
                val iconMarginVertical = (viewHolder.itemView.height - deleteIcon.intrinsicHeight) / 2

                if (dX > 0) {
                    colorDrawableBackground.setBounds(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                    deleteIcon.setBounds(itemView.left + iconMarginVertical, itemView.top + iconMarginVertical,
                        itemView.left + iconMarginVertical + deleteIcon.intrinsicWidth, itemView.bottom - iconMarginVertical)
                } else {
                    colorDrawableBackground.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                    deleteIcon.setBounds(itemView.right - iconMarginVertical - deleteIcon.intrinsicWidth, itemView.top + iconMarginVertical,
                        itemView.right - iconMarginVertical, itemView.bottom - iconMarginVertical)
                    deleteIcon.level = 0
                }

                colorDrawableBackground.draw(c)

                c.save()

                if (dX > 0)
                    c.clipRect(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                else
                    c.clipRect(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)

                deleteIcon.draw(c)

                c.restore()

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
            itemTouchHelper.attachToRecyclerView(orderHistoryRecyclerView)

        // Apply everything to the recycler view
        orderHistoryRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            itemAdapter = OrderItemAdapter(orderHistory, context, sharedPref)
            adapter = itemAdapter

            // Little line between rows
            // addItemDecoration(DividerItemDecoration(this.context,DividerItemDecoration.VERTICAL))
        }

    }

    // After all the querys have been received
    private fun displayOrders() {

        orderHistory.sort()

        // Print received items
        for (i in 0 until orderHistory.size) {
            // Log.d("MyTag", "Order is: " + orderHistory[i].foodItems + " Type: "+ orderHistory[i].orderType + " Date is " + orderHistory[i].orderTime.toString())
        }

        if (orderHistory.isEmpty()) {
            emptyMsgTextView.text = "You have no pending orders."
        }


        // Hide loading bar and update data
        orderHistoryProgressBar.visibility = View.INVISIBLE
        itemAdapter?.notifyData(orderHistory)
        // Log.d("MyTag","Order History Size : " + orderHistory.size)
    }


    private fun classNameSwitcher(classname : String) : String {
        return when (classname) {
            "PackedLunch" -> "Packed Lunch"
            "HotLunch" -> "Hot Lunch"
            else -> "Dinner"
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    public fun back(view : View) {
        onBackPressed()
    }

}